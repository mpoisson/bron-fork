#!/usr/bin/env python3

import json
import os.path

import networkx as nx

from utils.bron_utils import load_graph_network

BRON_USERNAME = "root"
BRON_PASSWORD = "changeme"
BRON_SERVER_IP = "127.0.0.1"
DB = "BRON"

ORIGINAL_ID = "original_id"


class Node:
    def __init__(self, node_id: str, sons: list):
        self.id = node_id
        self.sons = sons

    def __str__(self):
        s = "id" + self.id + "\n"
        for son in self.sons:
            s += "  " + str(son)
        return s

    def dico(self) -> dict:
        ret = {self.id: []}
        if self.sons:
            for son in self.sons:
                ret[self.id].append(son.dico())
        return ret


def dico_ids(dico_in: dict, nodes_data) -> dict:
    new_dict = {}
    for key in dico_in.keys():
        new_key = get_meaningful_key(key, nodes_data)
        new_dict[new_key] = []
        for sub_dico in dico_in[key]:
            sub_dico_key = list(sub_dico.keys())[0]
            # remove links CVE -> CPE and CAPEC -> CWE
            if not ("cpe_" in sub_dico_key and "cve_" in key) and not (
                "cwe_" in sub_dico_key and "capec_" in key
            ):
                new_dict[new_key].append(dico_ids(sub_dico, nodes_data))
    return new_dict


def get_meaningful_key(initial_key, nodes_data) -> str:
    if "cve_" in initial_key or "cpe_" in initial_key:
        return nodes_data[initial_key][ORIGINAL_ID]
    elif "cwe_" in initial_key:
        return (
            "CWE-"
            + nodes_data[initial_key][ORIGINAL_ID]
            + " "
            + nodes_data[initial_key]["name"]
        )
    elif "technique_" in initial_key or "capec_" in initial_key:
        base = (
            nodes_data[initial_key][ORIGINAL_ID] + " " + nodes_data[initial_key]["name"]
        )
        if "capec" in initial_key:
            base = "CAPEC-" + base
        return base
    elif "tactic" in initial_key:
        return (
            nodes_data[initial_key][ORIGINAL_ID] + " " + nodes_data[initial_key]["name"]
        )
    else:
        return initial_key + str(nodes_data[initial_key]).replace("\n", "").strip()


# n0 = Node("0", [Node("1", []), Node("2", [])])
# json.dumps(n0.dico(), indent=2)
# exit(0)


def build(src: Node, max_depth: int, graph: nx.DiGraph, depth: int) -> Node:
    if depth == 0 and "cve" in src.id:
        skip_cve_depth = 0
    else:
        # here we assume initial node is cpe, hence cve depth is 1 and not more
        skip_cve_depth = 1
    if depth >= max_depth:
        return src
    for neighbor in graph.neighbors(src.id):
        if "cve" in neighbor.lower() and depth + 1 > skip_cve_depth:
            continue
        src.sons.append(Node(neighbor, []))
        build(src.sons[-1], max_depth, graph, depth + 1)
    json.dumps(src.dico())


def node_original_id(tested_node):
    key, data = tested_node
    return data["original_id"]


def is_cpe_node(node) -> bool:
    original_id = node_original_id(node)
    if not original_id:
        return False
    else:
        return "cpe:" in original_id


def is_cve_node(node) -> bool:
    original_id = node_original_id(node)
    if not original_id:
        return False
    else:
        return "CVE-" in original_id


def get_path_edges_attr(path, graph):
    ret = []
    for (u, v) in zip(path[0:], path[1:]):
        # print(u)
        # print(v)
        # print(graph)
        # print(type(graph))
        # print(graph[u][v])
        ret.append((str(u).replace("\n", "."), graph[u][v]))
    ret.append(str(path[-1]))
    return ret


def get_path_edges_attr_2(path, graph):
    ret = []
    pathGraph = nx.path_graph(path)  # does not pass edges attributes

    # Read attributes from each edge
    for ea in pathGraph.edges():
        # print from_node, to_node, edge's attributes
        # print(ea, graph.edges[ea[0], ea[1]])
        ret.append(f"{ea}")


def log_path(nodes_list, nxgraph):
    print(f"src to dst: path length {len(nodes_list)}")
    print(nodes_list)
    print(f"{get_path_edges_attr_2(nodes_list, nxgraph)}")


def select_capec_paths(paths):
    cwe_paths = {}
    for last_node_id, full_path in paths.items():
        if "capec" in last_node_id and len(full_path) == 4:
            cwe_paths[last_node_id] = full_path
    return cwe_paths


def single_src_shortest_path(archi_graph: nx.DiGraph, src_node_id: str):
    paths = nx.single_source_shortest_path(archi_graph, src_node_id)
    capec_path = select_capec_paths(paths)
    if not capec_path:
        print("no path here")
        return
    nodes_data = {}
    for node_list in capec_path.values():
        print("path", "=" * 10)
        for node in node_list:
            if node in nodes_data.keys():
                print(nodes_data[node])
            else:
                for current_node_id, data in archi_graph.nodes(data=True):
                    if node == current_node_id:
                        nodes_data[node] = data
                        print(nodes_data[node])
    print("all paths logged")
    return nodes_data


def main():
    searched_node = "CVE-2021-44228"
    searched_node = "CVE-2020-10220"
    searched_node = "CVE-2022-22936"
    searched_node = "CVE-2018-1161"
    searched_node = "CVE-2023-1998"
    search_depth = 3


    path_file_out = f"my_bron_output_{searched_node}.json"

    if os.path.isfile(path_file_out):
        print(f"{path_file_out} already exist, nothing to be done")
        with open(path_file_out, "r") as f:
            return json.load(f)


    graph_path = "/home/manuel/Documents/BRON/data/attacks/BRON.json"
    print(f"loading graph at {graph_path} ...")
    nx_bron_graph = load_graph_network(graph_path)
    print("... graph loaded")
    # print(nx_bron_graph["cpe_19111"])
    # print(nx_bron_graph["cve_116943"])


    # searched_node = "cpe:2.3:a:microsoft:internet_explorer:8.0.6001:*:*:*:*:*:*:*"

    src_node: nx.classes.reportviews.NodeView | None = None
    for node in nx_bron_graph.nodes(data=True):
        if ("cpe:" in searched_node and is_cpe_node(node)) or (
            "CVE-" in searched_node and is_cve_node(node)
        ):
            if node[1][ORIGINAL_ID] == searched_node:
                src_node = node

    print(f"{src_node=}")
    if not src_node:
        exit(1)

    # single_src_shortest_path(nx_bron_graph, src_node[0])
    nodes_data = {}
    for node_id, data in nx_bron_graph.nodes(data=True):
        nodes_data[node_id] = data

    print("###" * 10)
    n0 = Node(src_node[0], [])
    build(n0, search_depth, nx_bron_graph, 0)
    dico_data = dico_ids(n0.dico(), nodes_data)
    str_tree = json.dumps(dico_data, indent=2)
    with open(path_file_out, "w") as f:
        print(f"file '{path_file_out}' written")
        print(str_tree, file=f)
    print("###" * 10)
    return dico_data

    # print(type(nx_bron_graph), nx_bron_graph.nodes)


main()

# client = arango.ArangoClient(hosts=f"http://{BRON_SERVER_IP}:8529")
# print("client creation done")
# db = client.db(DB, username=BRON_USERNAME, password=BRON_PASSWORD, auth_method="basic")
# print("db connection done")
# cve_query = """
# FOR e IN cve
# FILTER e.original_id == "CVE-2013-5046"
#     RETURN e._id
# """
# cpe_query = """
# FOR e IN cpe
# FILTER e.original_id == "cpe:2.3:a:microsoft:internet_explorer:8.0.6001:*:*:*:*:*:*:*"
#     RETURN e._id
# """
#
# cve_to_cwe_query = """
#    FOR cve_entry IN cve
#   RETURN {
#     cve_nb: cve_entry.original_id,
#     cwe_linked: (FOR cwe IN CweCve
#              FILTER cwe._to == cve_entry._id
#              RETURN cwe._from)
#   }
# """
#
# """
# save queries
#
#    FOR cve_entry IN cve
#   RETURN {
#     cve_nb: cve_entry.original_id,
#     cwe_linked: (FOR cwe IN CweCve
#              FILTER cwe._to == cve_entry._id
#              RETURN cwe._from)
#   }
#
#
# FOR cve_entry IN cve " +
#   LET a = (FOR x IN b.authors " +
#              FOR a IN authors FILTER x == a._id RETURN a) " +
#    RETURN { cve: cve_entry, authors: a }"
#
# """
# # see here https://www.arangodb.com/docs/stable/aql/examples-join.html
#
#
# query = cpe_query
#
# assert db.aql.validate(query)
# cursor = db.aql.execute(query)
# i = 0
# for c in cursor:
#     print(c)
#     i += 1
#     if i == 10:
#         exit(0)
