#!/usr/bin/env bash

sudo systemctl start docker
docker-compose up -d

echo "web interface here http://localhost:8529/_db/BRON/"
echo "user: root, pwd: changeme"
echo "work with ./tutorials/my_bron_usage.py"
echo "to stop run: docker-compose stop"
# /home/mpoisson/Documents/BRON/venv/bin/python /home/mpoisson/Documents/BRON/tutorials/my_bron_usage.py